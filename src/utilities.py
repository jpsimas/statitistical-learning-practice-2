def genDimsDetail(n, x):
    ret = []
    
    for j in range(1, n + 1):
        newDim = x + (j,)
        ret.append(newDim)
        
    return ret

def genDims(n, l, dimList = [()]):
    if(l == 0):
        return []
    
    ret = []
    for x in dimList:
        ret += genDimsDetail(n, x)
        
    return ret + genDims(n, l - 1, ret)

def paramOrder(dataRaw, metadata, attr_labels):
    ## TEST
    # calculate probability of >50k given each class label and order
    # numerical values by increasing probability of earning >50k
    freqs = []
    count = []
    for i in range(0, len(attr_labels)):
        freqs.append({"?" : 0})
        count.append({"?" : 0})
        for label in metadata[attr_labels[i]]:
            freqs[i][label] = 0
            count[i][label] = 0

    for i in range(0, len(attr_labels)):
        if(len(metadata[attr_labels[i]]) != 1):#if not continuous
            for j in range(0, dataRaw.shape[0]):
                count[i][dataRaw[j, i]] += 1
                if(dataRaw[j, -1] == metadata[attr_labels[-1]][0]):#if >50k
                    freqs[i][dataRaw[j, i]] += 1

            freqs[i]["?"] /= (count[i]["?"] + 1e-6)
            for label in metadata[attr_labels[i]]:
                freqs[i][label] /= (count[i][label] + 1e-6)         

    print(freqs)

    dicts = []
    for i in range(0, len(attr_labels)):
        dicts.append({})
        if(len(metadata[attr_labels[i]]) != 1):#if not continuous
            labels = ["?"]
            labels = labels + metadata[attr_labels[i]]
            labelFreqs = list(map(freqs[i].get, labels))
            perm = np.argsort(labelFreqs)
            for j in range(0, len(labels)):
                dicts[i][labels[j]] = perm[j]
        else:
            dicts[i]["?"] = -1

    for i in range(0, len(dicts)):
        print(dicts[i])

    return dicts
    ## TEST END
