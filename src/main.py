# PCS5024 - Practice 2
# Copyright (C) 2021  João Pedro de O. Simas
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
import numpy as np
import yaml
import time

from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import accuracy_score, precision_score, recall_score
from sklearn.model_selection import cross_val_score, RandomizedSearchCV, GridSearchCV

from sklearn.ensemble import GradientBoostingClassifier

import itertools

## map categorical values into numerical ones following the order they
## were given in the dictionaries
def processData(dataRaw, dicts):
    data = np.zeros((dataRaw.shape[0], dataRaw.shape[1]), dtype=float);

    # Build numerical dataset matrix (and transpose data)
    for i in range(0, dataRaw.shape[1]):
        for j in range(0, dataRaw.shape[0]):
            if(len(dicts[i]) != 1 or dataRaw[j, i] == "?"):
                data[j, i] = dicts[i][dataRaw[j, i]]
            else:
                data[j, i] = float(dataRaw[j, i])

    return data

## read data and metadata from files, map categorical values into
## numerical ones according to the order given in the metadata and
## finally normalize it so each attribute has zero main and unit variance.
def readData(path, prefix):
    ## Load Metadata
    file = open(path + prefix + ".yaml")
    metadata = yaml.load(file, Loader=yaml.FullLoader)
    file.close()
    
    ## Build attribute label name list
    attr_labels = []
    for key in metadata.keys():
        attr_labels.append(key)

    ## Load Data
    extension = ".data"
    dataRaw = np.genfromtxt(path + prefix + extension, delimiter=',', dtype="|U", autostrip=True)
                
    ## Build dictionaries for string labels to numerical values
    ## translation for each attribute
    dicts = []
    i = 0
    for key in metadata.keys():
        dicts.append({"?" : -1})
        j = 0
        if(len(metadata[key]) != 1): #if not continuous
            for label in metadata[key]:
                dicts[i][label] = j
                j += 1
        i += 1    

    # store label mappings to yaml file
    file = open("../out/dicts.yaml", "w")
    yaml.dump(dicts, file)
    file.close()

    # map categorical values using dictionary
    data = processData(dataRaw, dicts)

    #  Scaling factor vectors
    s = np.zeros((data.shape[1], 1), dtype=float);# std dev
    m = np.zeros((data.shape[1], 1), dtype=float);# mean
    
    ## Normalize/unbias columns (Except class colum)
    for i in range(0, data.shape[1] - 1):
        m[i] = np.mean(data[:, i])
        s[i] = np.std(data[:, i]) + 1e-12
        data[:, i] = (data[:, i] - m[i])/s[i]

    extension = ".test"

    dataTestRaw = np.genfromtxt(path + prefix + extension, delimiter=',', dtype="|U", autostrip=True, skip_header=1)
    dataTest = processData(dataTestRaw, dicts)
    
    # scale data
    for i in range(0, dataTest.shape[1] - 1):
        dataTest[:, i] = (dataTest[:, i] - m[i])/s[i]

    return (data, dataTest, attr_labels)

## calculate the mean accuracies from the obtained values from nFold
## cross-validation of some classifier model
def cross_val_mean_acc(model, nFold):
    accCV = cross_val_score(model, data[:, 0:(data.shape[1] - 1)], data[:, data.shape[1] - 1].astype(int), cv = nFold)
    acc = np.mean(accCV)
    return acc

## Main Function

## Print Copyright Notice
print("practice1  Copyright (C) 2021  João Pedro de Omena Simas\n"\
      "This program comes with ABSOLUTELY NO WARRANTY;\n"\
      "This is free software, and you are welcome to redistribute it\n"\
      "under certain conditions;\n")

plt.close("all")

path = "../data/"
prefix = "adult"

(data, dataTest, attr_labels) = readData(path, prefix)

## Plot Correlation Heat Map
corr = np.corrcoef(data.T)

plt.matshow(np.abs(corr), cmap = "hot")
plt.colorbar()
plt.xticks(ticks = range(0, len(attr_labels)), labels = attr_labels, rotation = 90)
plt.yticks(ticks = range(0, len(attr_labels)), labels = attr_labels)

## Attribute Selection
# remove attributes with correlation < 1e-2 with the output
excluded_cols = [] #force exclude redundant attribute education
for i in range(corr.shape[0] - 1, -1, -1):
    if(np.abs(corr[corr.shape[0] - 1, i]) < 1e-2):
        excluded_cols.append(i)

for i in excluded_cols:
    print("Excluded attribute " + str(i) + ": " + attr_labels[i] + ". corr: {:e}".format(corr[corr.shape[0] - 1, i]))
    
    data = data[:, np.r_[0:i, (i + 1):data.shape[1]]]
    dataTest = dataTest[:, np.r_[0:i, (i + 1):dataTest.shape[1]]]
    
    attr_labels.pop(i)

#compute covariance
corr = np.corrcoef(data.T)
    
# Plot Correlation Heat Map with attributes removed
plt.matshow(np.abs(corr), cmap = "hot")
plt.colorbar()
plt.xticks(ticks = range(0, len(attr_labels)), labels = attr_labels, rotation = 90)
plt.yticks(ticks = range(0, len(attr_labels)), labels = attr_labels)

## Train and validate models

# Naive Bayes
print("Naive Bayes:")

# Validate with test data set
modelGNB = GaussianNB()
modelGNB.fit(data[:, 0:(data.shape[1] - 1)], data[:, data.shape[1] - 1].astype(int))

#Figures of Merit
classPred = modelGNB.predict(dataTest[:, 0:(dataTest.shape[1] - 1)])
accuracy = accuracy_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)
precision = precision_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)
recall = recall_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)

print("Accuracy: {}".format(accuracy))
print("Precision: {}".format(precision))
print("Recall: {}".format(recall))

file = open("../out/gnbResults.yaml", "w")
yaml.dump({"accuracy" : float(accuracy), "precision" : float(precision), "recall" : float(recall)}, file)
file.close()

#plt.show()

# SVM
#Hyperparameter Optimization

# check if there are stored results
foundResults = True
try:
    file = open("../out/svmParams.yaml")
    params = yaml.load(file, Loader=yaml.Loader)

    C = float(params["C"])
    penalty = params["penalty"]
    
    file.close()
except IOError:
    foundResults = False

if(not foundResults):
    optSVM = GridSearchCV(
        LinearSVC(random_state = 0, max_iter = 10000, loss = "squared_hinge", dual = False),
        {'C': np.logspace(-3, 2, 12),
         'penalty' : ["l1", "l2"]},
        #    n_iter=5,
    cv=3,
        verbose = 3
    )

    optSVM.fit(data[:, 0:(data.shape[1] - 1)], data[:, data.shape[1] - 1].astype(int))

    print("optimal params: {}".format(optSVM.best_params_))
    print("cross val acc {}".format(optSVM.best_score_))

    C = optSVM.best_params_["C"]
    penalty = optSVM.best_params_["penalty"]
    
    # store parameters to yaml file
    file = open("../out/svmParams.yaml", "w")
    params = optSVM.best_params_
    params["accCv"] = float(optSVM.best_score_)
    yaml.dump(params, file)
    file.close()
else:
    print("SVM parameters found. Skipping optimization. To re-run delete ../out/svmParams.yaml")
    
#Fit model

modelSVM = LinearSVC(random_state = 0, max_iter = 10000, loss = "squared_hinge", dual = False, C = C, penalty = penalty)
modelSVM.fit(data[:, 0:(data.shape[1] - 1)], data[:, data.shape[1] - 1].astype(int))

#Figures of Merit
classPred = modelSVM.predict(dataTest[:, 0:(dataTest.shape[1] - 1)])
accuracy = accuracy_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)
precision = precision_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)
recall = recall_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)

print("Accuracy: {}".format(accuracy))
print("Precision: {}".format(precision))
print("Recall: {}".format(recall))

file = open("../out/svmResults.yaml", "w")
yaml.dump({"accuracy" : float(accuracy), "precision" : float(precision), "recall" : float(recall)}, file)
file.close()

# Logistic Regression
#Hyperparameter Optimization

# check if there are stored results
foundResults = True
try:
    file = open("../out/logisticParams.yaml")
    params = yaml.load(file, Loader=yaml.Loader)

    C = float(params["C"])
    l1_ratio = params["l1_ratio"]
    
    file.close()
except IOError:
    foundResults = False

if(not foundResults):
    optLogistic = GridSearchCV(
        LogisticRegression(random_state = 0, max_iter = 10000, dual = False, penalty = "elasticnet", solver = "saga"),
        {'C': np.logspace(-4, 0, 6),
         'l1_ratio': np.linspace(0, 1, 5)},
        #    n_iter=5,
    cv=3,
        verbose = 3
    )

    optLogistic.fit(data[:, 0:(data.shape[1] - 1)], data[:, data.shape[1] - 1].astype(int))

    print("optimal params: {}".format(optLogistic.best_params_))
    print("cross val acc {}".format(optLogistic.best_score_))

    C = optLogistic.best_params_["C"]
    l1_ratio = optLogistic.best_params_["l1_ratio"]
    
    # store parameters to yaml file
    file = open("../out/logisticParams.yaml", "w")
    params = optLogistic.best_params_
    params["accCv"] = float(optLogistic.best_score_)
    yaml.dump(params, file)
    file.close()
else:
    print("Logistic Regression parameters found. Skipping optimization. To re-run delete ../out/logisticParams.yaml")
    
#Fit model

modelLogistic = LogisticRegression(random_state = 0, max_iter = 10000, dual = False, penalty = "elasticnet", solver = "saga", C = C, l1_ratio = l1_ratio)
modelLogistic.fit(data[:, 0:(data.shape[1] - 1)], data[:, data.shape[1] - 1].astype(int))

#Figures of Merit
classPred = modelLogistic.predict(dataTest[:, 0:(dataTest.shape[1] - 1)])
accuracy = accuracy_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)
precision = precision_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)
recall = recall_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)

print("Accuracy: {}".format(accuracy))
print("Precision: {}".format(precision))
print("Recall: {}".format(recall))

file = open("../out/logisticResults.yaml", "w")
yaml.dump({"accuracy" : float(accuracy), "precision" : float(precision), "recall" : float(recall)}, file)
file.close()

# Gradient Boost
#Hyperparameter Optimization

# check if there are stored results
foundResults = True
try:
    file = open("../out/gbParams.yaml")
    params = yaml.load(file, Loader=yaml.Loader)

    max_depth = int(params["max_depth"])
    min_samples_split = int(params["min_samples_split"])
    min_samples_leaf = int(params["min_samples_leaf"])
    max_features = int(params["max_features"])
    
    file.close()
except IOError:
    foundResults = False

if(not foundResults):
    optGB = GridSearchCV(
        GradientBoostingClassifier(random_state = 0), 
        {'max_depth': range(3, 9),
         'min_samples_split': range(40, 80, 20),
         'min_samples_leaf': range(3, 8),
         'max_features': range(9, 12)
         },
        verbose = 3,
        cv = 3)
    optGB.fit(data[:, 0:(data.shape[1] - 1)], data[:, data.shape[1] - 1].astype(int))
    
    print("optimal params: {}".format(optGB.best_params_))
    print("cross val acc {}".format(optGB.best_score_))

    max_depth = optGB.best_params_["max_depth"]
    min_samples_split = optGB.best_params_["min_samples_split"]
    min_samples_leaf = optGB.best_params_["min_samples_leaf"]
    max_features = optGB.best_params_["max_features"]

    # store parameters to yaml file
    file = open("../out/gbParams.yaml", "w")
    params = optGB.best_params_
    params["accCv"] = float(optGB.best_score_)
    yaml.dump(params, file)
    file.close()
else:
    print("Gradient Boost parameters found. Skipping optimization. To re-run delete ../out/gbParams.yaml")
    
#Fit model

modelGB = GradientBoostingClassifier(random_state = 0, max_depth = max_depth, min_samples_split = min_samples_split, min_samples_leaf = min_samples_leaf, max_features = max_features)
modelGB.fit(data[:, 0:(data.shape[1] - 1)], data[:, data.shape[1] - 1].astype(int))

#Figures of Merit
classPred = modelGB.predict(dataTest[:, 0:(dataTest.shape[1] - 1)])
accuracy = accuracy_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)
precision = precision_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)
recall = recall_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)

print("Accuracy: {}".format(accuracy))
print("Precision: {}".format(precision))
print("Recall: {}".format(recall))

file = open("../out/gbResults.yaml", "w")
yaml.dump({"accuracy" : float(accuracy), "precision" : float(precision), "recall" : float(recall)}, file)
file.close()
